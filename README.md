# API de Gestión de Productos

Este proyecto es una API de gestión de productos desarrollada con Symfony.

## Requisitos

- Docker y Docker Compose

## Instalación

1. Clona el repositorio en tu máquina local.

git clone https://gitlab.com/raul.bueno1977/rgb_test.git

cd rgb_test

2. Instalar la API

make install

3. El usuario por defecto es:

admin@localhost.local / adminpass1

4. Los test los ejecutamos con:

make execute-test

5. Paramos todo:

make stop

6. Renaudar de nuevo:

make start


8. Decisiones:

He optado por utilizar la versión más reciente de Symfony 6.3 instalando, solamente las dependecias necesarias.

Como base de datos he optado por MariaDB Jammy con PHPMyAdmin para poder gestionar las bases de datos.

Como servidor web he puesto Nginx que me gusta mucho más que Apache.

En el docker-compose.yml está todo.

Los test están con PHPUnit.

Desde el Makefile se puede instalar, ejecutar, parar y ejecutar los test de la API.

El API gestiona los productos, impuestos, categorías y marcas. Me hubiera gustado poner más endpoints pero por tiempo no he podido.

Con clonar este repositorio tenemos todo lo suficiente para corra la API en cualquier equipo con Docker y docker-compose, Linux mejor.

IMPORTANTE PARA EL BUEN FUNCIONAMIENTO HACER "make install", para los test puedes hacer "make execute-test"
