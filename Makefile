#!make

DOCKER_COMPOSE_FILE?=./docker-compose.yml
DOCKER_COMPOSE=docker-compose -f ${DOCKER_COMPOSE_FILE} 

start:
	${DOCKER_COMPOSE} up -d --build ${args}
	@echo "OK!"
	@echo "url: http://localhost"
	@echo "Initial credentials: admin@localhost.local/adminpass1"

install:
	@if [ -e "./app/var" ]; then \
		echo "cache and log dir created yet"; \
	else \
		mkdir ./app/var && mkdir ./app/var/cache && mkdir ./app/var/log && chmod 777 -R ./app/var; \
	fi
	@if [ -e "./app/public/images" ]; then \
		echo "images dir created yet"; \
	else \
		mkdir ./app/public/images && chmod 777 -R ./app/public/images; \
	fi
	${DOCKER_COMPOSE} up -d --build ${args}
	sleep 5;
	docker exec php-fpm composer install 
	docker exec php-fpm php bin/console doctrine:schema:update --force
	docker exec php-fpm php bin/console doctrine:database:create --env=test
	docker exec php-fpm php bin/console doctrine:schema:update  --env=test --force
	sleep 5
	docker exec php-fpm php bin/console app:initialize
	docker exec php-fpm php bin/console app:initialize --env=test
	@echo "OK!"
	@echo "url login: http://localhost/api/login_check"
	@echo "Initial credentials: admin@localhost.local/adminpass1"

execute-test:
	docker exec php-fpm php bin/phpunit --debug

stop:
	docker exec php-fpm rm -Rf ./app/var/cache/dev
	docker exec php-fpm rm -Rf ./app/var/cache/prod
	${DOCKER_COMPOSE} down ${args}
	@echo "Bye"

bash:
	${DOCKER_COMPOSE} exec php-fpm /bin/bash