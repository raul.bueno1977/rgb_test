<?php

// src/EventListener/ExceptionListener.php
namespace App\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        $response = new JsonResponse();

        // Aquí puedes configurar la respuesta según tu necesidad
        $response->setData([
            'message' => $exception->getMessage(),
            'code' => $exception->getCode(),
        ]);

        // HttpExceptionInterface es una interfaz especial que contiene
        // el código de estado HTTP y los encabezados de la respuesta
        if ($exception instanceof HttpExceptionInterface) {
            $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());
        } else {
            $response->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        // Envia la respuesta modificada al evento
        $event->setResponse($response);
    }
}
