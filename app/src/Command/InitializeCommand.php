<?php

namespace App\Command;

use App\Entity\Brand;
use App\Entity\Category;
use App\Entity\Product;
use App\Entity\Taxes;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(
    name: 'app:initialize',
    description: 'Add a short description for your command',
)]
class InitializeCommand extends Command
{

    /** @var EntityManagerInterface */
    private $em;

    /** @var UserPasswordHasherInterface */
    private $passwordHasher;

    public function __construct(EntityManagerInterface $em, UserPasswordHasherInterface $passwordHasher, string $name = null)
    {
        $this->em = $em;
        $this->passwordHasher = $passwordHasher;

        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        
        
        $user = new User();

        $user
            ->setName("Administrator")
            ->setEmail("admin@localhost.local")
            ->setRoles(["ROLE_ADMIN"])
            ->setPassword($this->passwordHasher->hashPassword($user, "adminpass1"))
        ;

        $this->em->persist($user);
        $this->em->flush();
       
        
        $taxes_type = [4, 10, 21];

        foreach ($taxes_type as $item) {
            $tax = new Taxes();

            $tax
                ->setValue((float)$item)
                ->setDescription("IVA " . (String)$item . "%")
                ->setEnabled(true);
            
            $this->em->persist($tax);
            $this->em->flush();
        }
        
    
        $category = new Category();

        $category->setName("First category");

        $this->em->persist($category);
        $this->em->flush();

            
        $brand = new Brand();

        $brand->setName("ACME");

        $this->em->persist($brand);
        $this->em->flush();

        
        
        $category = $this->em->getRepository(Category::class)->findAll();
        $brand = $this->em->getRepository(Brand::class)->findAll();

        for ($i = 0; $i < 20; $i++) {
            $product = new Product();

            $product
                ->setCategory($category[0])
                ->setBrand($brand[0])
                ->setName('Product Test ' . (string)$i)
                ->setDescription('Product Description ' . (string)$i)
                ->setPrice(5.00 * (float)$i)
                ->setReference(uniqid())
            ;

            $this->em->persist($product);
            $this->em->flush();
        }
    

        $io->success('Application initialized succesfully!!!');

        return Command::SUCCESS;
        
    }
}
