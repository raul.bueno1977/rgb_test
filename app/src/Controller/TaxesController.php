<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Taxes;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TaxesController extends AbstractController
{

    /**
     * Get all taxes
     *
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function findAll(EntityManagerInterface $em): JsonResponse {
        $taxes = $em->getRepository(Taxes::class)->findAll();

        return $this->json($taxes, 200, [], ['groups' => 'taxes:read']);
    }

    /**
     * Create new tax
     *
     * @param EntityManagerInterface $em
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return JsonResponse
     */
    public function create(EntityManagerInterface $em, Request $request, ValidatorInterface $validator): JsonResponse {
        $data = json_decode($request->getContent(), true);

        $tax = new Taxes();
        $tax
            ->setEnabled($data['enabled'])
            ->setValue($data['value'])
            ->setDescription($data['description'])
        ;

        $errors = $validator->validate($tax);

        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }

            return $this->json([
                'errors' => $errorMessages,
            ], 400);
        }

        $em->persist($tax);
        $em->flush();

        return $this->json($tax, 200, [], ['groups' => 'taxes:read']);
    }

    /**
     * Update tax
     *
     * @param integer $id
     * @param EntityManagerInterface $em
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return JsonResponse
     */
    public function update(int $id, EntityManagerInterface $em, Request $request, ValidatorInterface $validator): JsonResponse {
        $data = json_decode($request->getContent(), true);

        $tax = $em->getRepository(Taxes::class)->find($id);

        if (!$tax) {
            return $this->json([
                'error' => 'Tax not found',
            ], 404);
        }

        if (isset($data['value'])) {
            $tax->setValue($data['value']);
        }
        
        if (isset($data['enabled'])) {
            $tax->setEnabled($data['enabled']);
        }

        if (isset($data['description'])) {
            $tax->setDescription($data['description']);
        }

        $errors = $validator->validate($tax);

        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }

            return $this->json([
                'errors' => $errorMessages,
            ], 400);
        }

        $em->persist($tax);
        $em->flush();

        return $this->json($tax, 200, [], ['groups' => 'taxes:read']);
    }

    /**
     * Delete a tax
     *
     * @param integer $id
     * @param EntityManagerInterface $em
     * @return void
     */
    public function delete(int $id, EntityManagerInterface $em) {
        $tax = $em->getRepository(Taxes::class)->find($id);

        if (!$tax) {
            return $this->json([
                'error' => 'Tax not found',
            ], 404);
        }

        $products = $em->getRepository(Product::class)->findOneBy([
            'tax' => $tax
        ]);

        if ($products) {
            return $this->json([
                'error' => 'There are products with that tax',
            ], 403);
        }

        $em->remove($tax);
        $em->flush();

        return $this->json([
            'message' => 'Tax deleted successfully',
        ], 200);
    }
}
