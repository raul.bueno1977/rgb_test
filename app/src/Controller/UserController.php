<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserController extends AbstractController
{




    public function findAll(EntityManagerInterface $em): JsonResponse {
        $users = $em->getRepository(User::class)->findAll();
        
        return $this->json($users, 200, [], ['groups' => 'user:read']);
    }

    /**
     * Create new user.
     *
     * @param EntityManagerInterface $em 
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param UserPasswordHasherInterface $passwordHasher
     * @return JsonResponse
     */
    public function create(EntityManagerInterface $em, Request $request, ValidatorInterface $validator, UserPasswordHasherInterface $passwordHasher): JsonResponse {
        $data = json_decode($request->getContent(), true);

        $user = new User();
        $user
            ->setEmail($data['email'])
            ->setName($data['name'])
            ->setRoles($data['roles'])
        ;

        $plainPassword = $data['password'];
        $encodedPassword = $passwordHasher->hashPassword($user, $plainPassword);
        $user->setPassword($encodedPassword);

        $errors = $validator->validate($user);

        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }

            return $this->json([
                'errors' => $errorMessages,
            ], 400);
        }

        $em->persist($user);
        $em->flush();

        return $this->json($user, 200, [], ['groups' => 'user:read']);
    }

    /**
     * Update a user
     *
     * @param integer $id
     * @param EntityManagerInterface $em
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param UserPasswordHasherInterface $passwordHasher
     * @return JsonResponse
     */
    public function update(int $id, EntityManagerInterface $em, Request $request, ValidatorInterface $validator, UserPasswordHasherInterface $passwordHasher): JsonResponse {
        $data = json_decode($request->getContent(), true);

        $user = $em->getRepository(User::class)->find($id);

        if (!$user) {
            return $this->json([
                'error' => 'User not found',
            ], 404);
        }

        if (isset($data['email']) && trim($data['email'] != $user->getEmail())) {
            return $this->json([
                'error' => 'You can\'t change the user email',
            ], 403);
        }
        
        if (isset($data['name'])) {
            $user->setName($data['name']);
        }
    
        if (isset($data['roles'])) {
            if (count($data['roles']) == 1 && $data['roles'][0] == 'ROLE_USER') {
                $admin_users = $em->getRepository(User::class)->findAdmins();

                $is_admin = false;

                foreach ($user->getRoles() as $role) {
                    if ($role == "ROLE_ADMIN") {
                        $is_admin = true;
                        break;
                    }
                }

                if (count($admin_users) == 1 && $is_admin) {
                    return $this->json([
                        'error' => 'Always we have to have one admin user at least',
                    ], 403);
                }
            } 
            
            $user->setRoles($data['roles']);    
        }
    
        if (isset($data['password'])) {
            $plainPassword = $data['password'];
            $encodedPassword = $passwordHasher->hashPassword($user, $plainPassword);
            $user->setPassword($encodedPassword);
        }
    
        $errors = $validator->validate($user);

        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }

            return $this->json([
                'errors' => $errorMessages,
            ], 400);
        }

        $em->persist($user);
        $em->flush();

        return $this->json($user, 200, [], ['groups' => 'user:read']);
    }

    /**
     * Undocumented function
     *
     * @param integer $id
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function delete(int $id, EntityManagerInterface $em): JsonResponse {
        $user = $em->getRepository(User::class)->find($id);

        if (!$user) {
            return $this->json([
                'error' => 'User not found',
            ], 404);
        }

        $is_admin = false;

        foreach ($user->getRoles() as $role) {
            if ($role == "ROLE_ADMIN") {
                $is_admin = true;
                break;
            }
        }

        //always there is one admin user
        $admin_users = $em->getRepository(User::class)->findAdmins();

        if (count($admin_users) == 1 && $is_admin) {
            return $this->json([
                'error' => 'Always we have to have one admin user at least',
            ], 403);
        }

        $em->remove($user);
        $em->flush();

        return $this->json([
            'message' => 'User deleted successfully',
        ], 200);
    }

    /**
     * Return one user by the email address
     *
     * @param string $email
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function findByEmail(string $email, EntityManagerInterface $em): JsonResponse {
        $user = $em->getRepository(User::class)->findOneBy([
            'email' => trim($email)
        ]);

        if (!$user) {
            return $this->json([
                'error' => 'User not found',
            ], 404);
        }

        return $this->json($user, 200, [], ['groups' => 'user:read']);
    }

    /**
     * Return a user by the id
     *
     * @param integer $id
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function findById(int $id, EntityManagerInterface $em): JsonResponse {
        $user = $em->getRepository(User::class)->find($id);

        if (!$user) {
            return $this->json([
                'error' => 'User not found',
            ], 404);
        }

        return $this->json($user, 200, [], ['groups' => 'user:read']);
    }
        
}
