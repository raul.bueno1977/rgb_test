<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CategoryController extends AbstractController
{
    /**
     * Find categories
     *
     * @param Request $request send filter and father_id optional
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function find(Request $request, EntityManagerInterface $em): JsonResponse
    {

        $categories = array();

        if ($request->getMethod() === "GET") {
            $categories = $em->getRepository(Category::class)->findBy([], ['name' => 'ASC']);
        }
        
        if ($request->getMethod() === "POST") {
            $data = json_decode($request->getContent(), true);

            $filter = null;
            
            if (isset($data['filter'])) {
                $filter = $data['filter'];
            }

            $father_id = null;

            if (isset($data['father_id'])) {
                $father_id = $data['father_id'];
            }

            $categories = $em->getRepository(Category::class)->findByFilterAndFather($filter, $father_id);
        }

        return $this->json($categories, 200, [], ['groups' => 'categories:read']);
    }

    /**
     * Create new ctegory
     *
     * @param EntityManagerInterface $em
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return JsonResponse
     */
    public function create(EntityManagerInterface $em, Request $request, ValidatorInterface $validator): JsonResponse {
        $data = json_decode($request->getContent(), true);

        $father = null;
        
        if (isset($data['father'])) { 
            $father = $em->getRepository(Category::class)->find((int)$data['father']);

            if (!$father) {
                return $this->json(['message' => 'Category father is not found'], 404);
            }
        }

        $category = new Category();
        $category
            ->setName($data['name'])
            ->setFather($father)
        ;

        $errors = $validator->validate($category);

        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }

            return $this->json([
                'errors' => $errorMessages,
            ], 400);
        }

        $em->persist($category);
        $em->flush();

        return $this->json($category, 200, [], ['groups' => 'categories:read']);
    }

    /**
     * Update category
     *
     * @param integer $id
     * @param EntityManagerInterface $em
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return JsonResponse
     */
    public function update(int $id, EntityManagerInterface $em, Request $request, ValidatorInterface $validator): JsonResponse {
        $data = json_decode($request->getContent(), true);

        $category = $em->getRepository(Category::class)->find($id);

        if (!$category) {
            return $this->json([
                'error' => 'Category not found',
            ], 404);
        }

        $father = null;
        
        if (isset($data['father'])) { 
            $father = $em->getRepository(Category::class)->find((int)$data['father']);

            if (!$father) {
                return $this->json(['message' => 'Category father is not found'], 404);
            }
        }

        if ($father) {
            $category->setFather($category);
        }
        
        if (isset($data['name'])) {
            $category->setName($data['name']);
        }

        $errors = $validator->validate($category);

        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }

            return $this->json([
                'errors' => $errorMessages,
            ], 400);
        }

        $em->persist($category);
        $em->flush();

        return $this->json($category, 200, [], ['groups' => 'categories:read']);
    }

    /**
     * Delete a category
     *
     * @param integer $id
     * @param EntityManagerInterface $em
     * @return void
     */
    public function delete(int $id, EntityManagerInterface $em) {
        $category = $em->getRepository(Category::class)->find($id);

        if (!$category) {
            return $this->json([
                'error' => 'Category not found',
            ], 404);
        }

        $products = $em->getRepository(Product::class)->findOneBy([
            'category' => $category
        ]);

        if ($products) {
            return $this->json([
                'error' => 'There are products with that category',
            ], 403);
        }


        $categories = $em->getRepository(Category::class)->findOneBy([
            'father' => $category
        ]);

        if ($categories) {
            return $this->json([
                'error' => 'There are categories with that category',
            ], 403);
        }

        $em->remove($category);
        $em->flush();

        return $this->json([
            'message' => 'Tax deleted successfully',
        ], 200);
    }
}
