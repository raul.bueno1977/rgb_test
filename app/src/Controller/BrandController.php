<?php

namespace App\Controller;

use App\Entity\Brand;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BrandController extends AbstractController
{
    /**
     * Find brands
     *
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function findAll(EntityManagerInterface $em): JsonResponse
    {

        $brands = $em->getRepository(Brand::class)->findAll();


        return $this->json($brands, 200, [], ['groups' => 'brands:read']);
    }

    /**
     * Create new brand
     *
     * @param EntityManagerInterface $em
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return JsonResponse
     */
    public function create(EntityManagerInterface $em, Request $request, ValidatorInterface $validator): JsonResponse {
        $data = json_decode($request->getContent(), true);

        $brand = new Brand();
        $brand->setName($data['name']);

        $errors = $validator->validate($brand);

        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }

            return $this->json([
                'errors' => $errorMessages,
            ], 400);
        }

        $em->persist($brand);
        $em->flush();

        return $this->json($brand, 200, [], ['groups' => 'brands:read']);
    }

    /**
     * Update brand
     *
     * @param integer $id
     * @param EntityManagerInterface $em
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return JsonResponse
     */
    public function update(int $id, EntityManagerInterface $em, Request $request, ValidatorInterface $validator): JsonResponse {
        $data = json_decode($request->getContent(), true);

        $brand = $em->getRepository(Brand::class)->find($id);

        if (!$brand) {
            return $this->json([
                'error' => 'Brand not found',
            ], 404);
        }
        
        if (isset($data['name'])) {
            $brand->setName($data['name']);
        }

        $errors = $validator->validate($brand);

        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }

            return $this->json([
                'errors' => $errorMessages,
            ], 400);
        }

        $em->persist($brand);
        $em->flush();

        return $this->json($brand, 200, [], ['groups' => 'brands:read']);
    }

    /**
     * Delete a brand
     *
     * @param integer $id
     * @param EntityManagerInterface $em
     * @return void
     */
    public function delete(int $id, EntityManagerInterface $em) {
        $brand = $em->getRepository(Brand::class)->find($id);

        if (!$brand) {
            return $this->json([
                'error' => 'Brand not found',
            ], 404);
        }

        $products = $em->getRepository(Product::class)->findOneBy([
            'brand' => $brand
        ]);

        if ($products) {
            return $this->json([
                'error' => 'There are products with that brand',
            ], 403);
        }

        $em->remove($brand);
        $em->flush();

        return $this->json([
            'message' => 'Tax deleted successfully',
        ], 200);
    }
}
