<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\Taxes;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductController extends AbstractController
{

    /**
     * Create new product
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function create(Request $request, EntityManagerInterface $em, ValidatorInterface $validator): JsonResponse
    {

        $data = json_decode($request->getContent(), true);

        $brand = null;
        $category = null;
        $tax = null;
        
        if (isset($data['brand'])) {
            $brand = $em->getRepository(Brand::class)->find((int)$data['brand']);
        }

        if (isset($data['category'])) {
            $category = $em->getRepository(Category::class)->find((int)$data['category']);
        }

        if (isset($data['tax'])) {
            $tax = $em->getRepository(Taxes::class)->findOneBy([
                'value' => (float)$data['tax']
            ]);

            if (!$tax) {
                $tax = $em->getRepository(Taxes::class)->find((int)$data['tax']);
            }
        }


        $product = new Product();

        $product
            ->setCategory($category)
            ->setBrand($brand)
            ->setTax($tax)
            ->setName(trim($data['name']))
            ->setDescription($data['description'])
            ->setPrice((float)$data['price'])
            ->setReference($data['reference'])
        ;

        $fileBase64 = isset($data['image']) ? $data['image'] : null;

        if ($fileBase64) {

            $uploadDirectory = $this->getParameter('images_directory');

            if (!file_exists($uploadDirectory)) {
                mkdir($uploadDirectory, 0777, true);
            }

            [$format, $fileData] = explode(';base64,', $fileBase64, 2);
            
            $fileExtension = explode('/', $format)[1];
            
            if ($fileExtension !== 'jpg' && $fileExtension !== 'jpeg' && $fileExtension !== 'png') {
                throw new \Exception('Invalid file format. Please upload a JPG or PNG image.');
            }

            $fileName = uniqid() . '.' . $fileExtension;
            
            $fileDecoded = base64_decode($fileData, true);

            if ($fileDecoded === false) {
                throw new \Exception('Invalid base64 encoding.');
            }

            file_put_contents($uploadDirectory . '/' . $fileName, $fileDecoded);
            
            $product->setImage($fileName);
        }

        $errors = $validator->validate($product);

        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }

            return $this->json([
                'errors' => $errorMessages,
            ], 400);
        }

        $em->persist($product);
        $em->flush();

        $defaultContext = [
            ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                return $object->getId();
            },
            ObjectNormalizer::IGNORED_ATTRIBUTES => ['created_at', 'updated_at'],
            DateTimeNormalizer::FORMAT_KEY => 'Y-m-d\TH:i:sP', 
        ];

        $normalizers = [
            new DateTimeNormalizer(),
            new ObjectNormalizer(null, null, null, null, null, null, $defaultContext),
        ];

        $serializer = new Serializer($normalizers, [new JsonEncoder()]);

        $productJson = $serializer->serialize($product, 'json');

        return new JsonResponse($productJson, 200, [], true);
    }

    /**
     * Modify product
     *
     * @param integer $id
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function update(int $id, Request $request, EntityManagerInterface $em, ValidatorInterface $validator): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $product = $em->getRepository(Product::class)->find($id);

        if (!$product) {
            return $this->json([
                'error' => 'Product not found',
            ], 404);
        }
    

        $brand = null;
        $category = null;
        $tax = null;
        
        if (isset($data['brand'])) {
            $brand = $em->getRepository(Brand::class)->find((int)$data['brand']);
            $product->setBrand($brand);
        }

        if (isset($data['category'])) {
            $category = $em->getRepository(Category::class)->find((int)$data['category']);
            $product->setCategory($category);
        }

        if (isset($data['tax'])) {
            $tax = $em->getRepository(Taxes::class)->findOneBy([
                'value' => (float)$data['tax']
            ]);

            if (!$tax) {
                $tax = $em->getRepository(Taxes::class)->find((int)$data['tax']);
                $product->setTax($tax);
            }else {
                $product->setTax($tax);
            }
        }

        if (isset($data['name'])) {
            $product->setName($data['name']);
        }

        if (isset($data['description'])) {
            $product->setName($data['description']);
        }

        if (isset($data['reference'])) {
            $product->setName($data['reference']);
        }

        if (isset($data['price'])) {
            $product->setName($data['price']);
        }

        $fileBase64 = isset($data['image']) ? $data['image'] : null;

        if ($fileBase64) {

            $uploadDirectory = $this->getParameter('images_directory');

            if (!file_exists($uploadDirectory)) {
                mkdir($uploadDirectory, 0777, true);
            }

            [$format, $fileData] = explode(';base64,', $fileBase64, 2);
            
            $fileExtension = explode('/', $format)[1];
            
            if ($fileExtension !== 'jpg' && $fileExtension !== 'jpeg' && $fileExtension !== 'png') {
                throw new \Exception('Invalid file format. Please upload a JPG or PNG image.');
            }

            $fileName = uniqid() . '.' . $fileExtension;
            
            $fileDecoded = base64_decode($fileData, true);

            if ($fileDecoded === false) {
                throw new \Exception('Invalid base64 encoding.');
            }

            file_put_contents($uploadDirectory . '/' . $fileName, $fileDecoded);
            
            if ($product->getImage()) {
                $filesystem = new Filesystem();
                $filesystem->remove($uploadDirectory . "/" . $product->getImage());
            }

            $product->setImage($fileName);
        }

        $errors = $validator->validate($product);

        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }

            return $this->json([
                'errors' => $errorMessages,
            ], 400);
        }

        $em->persist($product);
        $em->flush();

        $defaultContext = [
            ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                return $object->getId();
            },
            ObjectNormalizer::IGNORED_ATTRIBUTES => ['created_at', 'updated_at'],
            DateTimeNormalizer::FORMAT_KEY => 'Y-m-d\TH:i:sP', 
        ];

        $normalizers = [
            new DateTimeNormalizer(),
            new ObjectNormalizer(null, null, null, null, null, null, $defaultContext),
        ];

        $serializer = new Serializer($normalizers, [new JsonEncoder()]);

        $productJson = $serializer->serialize($product, 'json');

        return new JsonResponse($productJson, 200, [], true);
    }

    /**
     * Find product by reference
     *
     * @param string $reference
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function findByReference(string $reference, EntityManagerInterface $em): JsonResponse {
        $product = $em->getRepository(Product::class)->findOneBy([
            'reference' => $reference,
        ]);

        if (!$product) {
            return $this->json([
                'error' => 'Product not found',
            ], 404);
        }

        $defaultContext = [
            ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                return $object->getId();
            },
            ObjectNormalizer::IGNORED_ATTRIBUTES => ['created_at', 'updated_at'],
            DateTimeNormalizer::FORMAT_KEY => 'Y-m-d\TH:i:sP', 
        ];

        $normalizers = [
            new DateTimeNormalizer(),
            new ObjectNormalizer(null, null, null, null, null, null, $defaultContext),
        ];

        $serializer = new Serializer($normalizers, [new JsonEncoder()]);

        $productJson = $serializer->serialize($product, 'json');

        return new JsonResponse($productJson, 200, [], true);
    }

    /**
     * Find product by Id
     *
     * @param string $reference
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function findById(int $id, EntityManagerInterface $em): JsonResponse {
        $product = $em->getRepository(Product::class)->find($id);

        if (!$product) {
            return $this->json([
                'error' => 'Product not found',
            ], 404);
        }

        $defaultContext = [
            ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                return $object->getId();
            },
            ObjectNormalizer::IGNORED_ATTRIBUTES => ['created_at', 'updated_at'],
            DateTimeNormalizer::FORMAT_KEY => 'Y-m-d\TH:i:sP', 
        ];

        $normalizers = [
            new DateTimeNormalizer(),
            new ObjectNormalizer(null, null, null, null, null, null, $defaultContext),
        ];

        $serializer = new Serializer($normalizers, [new JsonEncoder()]);

        $productJson = $serializer->serialize($product, 'json');

        return new JsonResponse($productJson, 200, [], true);
    }

    /**
     * Find all products
     *
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function findAll(Request $request, EntityManagerInterface $em): JsonResponse {
        $page = (int)$request->query->get('page', 1); 
        $pageSize = (int)$this->getParameter('page_size');
        
        $query = $em->getRepository(Product::class)->findAllProducts($page, $pageSize);

        $paginator = new Paginator($query);

        $products = [];
        foreach($paginator as $product) {
            $products[] = $product;
        }

        $defaultContext = [
            ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                return $object->getId();
            },
            ObjectNormalizer::IGNORED_ATTRIBUTES => ['created_at', 'updated_at'],
            DateTimeNormalizer::FORMAT_KEY => 'Y-m-d\TH:i:sP', 
        ];

        $normalizers = [
            new DateTimeNormalizer(),
            new ObjectNormalizer(null, null, null, null, null, null, $defaultContext),
        ];

        $serializer = new Serializer($normalizers, [new JsonEncoder()]);

        $productsJson = $serializer->serialize($products, 'json');

        return new JsonResponse($productsJson, 200, [], true);
    }

    /**
     * Find all products by category
     *
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function findByCategory(int $category_id, Request $request, EntityManagerInterface $em): JsonResponse {
        $page = (int)$request->query->get('page', 1); 
        $pageSize = (int)$this->getParameter('page_size');
        
        $query = $em->getRepository(Product::class)->findProductsByCategory($category_id, $page, $pageSize);


        $paginator = new Paginator($query);

        $products = [];
        foreach($paginator as $product) {
            $products[] = $product;
        }

        $defaultContext = [
            ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                return $object->getId();
            },
            ObjectNormalizer::IGNORED_ATTRIBUTES => ['created_at', 'updated_at'],
            DateTimeNormalizer::FORMAT_KEY => 'Y-m-d\TH:i:sP', 
        ];

        $normalizers = [
            new DateTimeNormalizer(),
            new ObjectNormalizer(null, null, null, null, null, null, $defaultContext),
        ];

        $serializer = new Serializer($normalizers, [new JsonEncoder()]);

        $productsJson = $serializer->serialize($products, 'json');

        return new JsonResponse($productsJson, 200, [], true);
    }

    /**
     * Find all products by brand
     *
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function findByBrand(int $brand_id, Request $request, EntityManagerInterface $em): JsonResponse {
        $page = (int)$request->query->get('page', 1); 
        $pageSize = (int)$this->getParameter('page_size');
        
        $query = $em->getRepository(Product::class)->findProductsByBrand($brand_id, $page, $pageSize);

        $paginator = new Paginator($query);

        $products = [];
        foreach($paginator as $product) {
            $products[] = $product;
        }

        $defaultContext = [
            ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                return $object->getId();
            },
            ObjectNormalizer::IGNORED_ATTRIBUTES => ['created_at', 'updated_at'],
            DateTimeNormalizer::FORMAT_KEY => 'Y-m-d\TH:i:sP', 
        ];

        $normalizers = [
            new DateTimeNormalizer(),
            new ObjectNormalizer(null, null, null, null, null, null, $defaultContext),
        ];

        $serializer = new Serializer($normalizers, [new JsonEncoder()]);

        $productsJson = $serializer->serialize($products, 'json');

        return new JsonResponse($productsJson, 200, [], true);
    }

    /**
     * Delete product
     *
     * @param integer $id
     * @param EntityRepository $em
     * @return JsonResponse
     */
    public function delete(int $id, EntityManagerInterface $em): JsonResponse {
        $product = $em->getRepository(Product::class)->find($id);

        if (!$product) {
            return $this->json([
                'error' => 'Product not found',
            ], 404);
        }

        if ($product->getImage()) {
            $uploadDirectory = $this->getParameter('images_directory');

            $filesystem = new Filesystem();
            $filesystem->remove($uploadDirectory . "/" . $product->getImage());
        }

        $em->remove($product);
        $em->flush();

        return $this->json([
            'message' => 'Product deleted successfully',
        ], 200);
    }
}
