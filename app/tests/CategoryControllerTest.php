<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CategoryControllerTest extends WebTestCase
{
    private function getToken(KernelBrowser $client): string {
        $client->request('POST', '/api/login_check', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'username' => 'admin@localhost.local',
            'password' => 'adminpass1',
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        
        $data = json_decode($client->getResponse()->getContent(), true);
        return $data['token']; 
    }

    public function testCreateCategory(): void {
        $client = static::createClient();

        $token = $this->getToken($client);


        $client->request('POST', '/api/admin/categories/create', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
            'CONTENT_TYPE' => 'application/json',
        ], json_encode([
            'name' => 'Test category'
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode(),$client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $createdCategory = json_decode($content, true);

        $this->assertIsArray($createdCategory);

        $this->assertArrayHasKey('name', $createdCategory);
        $this->assertEquals('Test category', $createdCategory['name']);
    }

    public function testGetCategory(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/categories/find', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode(),$client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $categories = json_decode($content, true);

        $this->assertIsArray($categories);

        $this->assertArrayHasKey('id', $categories[0]);
        $this->assertArrayHasKey('name', $categories[0]);
    }

    

    public function testUpdateCategory(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/categories/find', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $categories = json_decode($content, true);

        $this->assertIsArray($categories);

        $categoryToUpdate = $categories[count($categories) - 1];

        $client->request('PUT', '/api/admin/categories/update/' . $categoryToUpdate['id'], [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
            'CONTENT_TYPE' => 'application/json',
        ], json_encode([
            'name' => 'Test category modified',
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), $client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $updatedCategory = json_decode($content, true);

        $this->assertArrayHasKey('name', $updatedCategory);
        $this->assertEquals('Test category modified', $updatedCategory['name']);
    }

    public function testDeleteCategory(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/categories/find', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $categories = json_decode($content, true);

        $this->assertIsArray($categories);

        $categoriesToSearch = $categories[count($categories) - 1];

        $client->request('DELETE', '/api/admin/categories/delete/' . $categoriesToSearch['id'], [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), $client->getResponse()->getContent());
    }

}

