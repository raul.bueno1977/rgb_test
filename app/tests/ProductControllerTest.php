<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductontrollerTest extends WebTestCase
{
    private function getToken(KernelBrowser $client): string {
        $client->request('POST', '/api/login_check', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'username' => 'admin@localhost.local',
            'password' => 'adminpass1',
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        
        $data = json_decode($client->getResponse()->getContent(), true);
        return $data['token']; 
    }

    public function testCreateProduct(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('POST', '/api/admin/categories/create', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
            'CONTENT_TYPE' => 'application/json',
        ], json_encode([
            'name' => 'Test category'
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode(),$client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $createdCategory = json_decode($content, true);

        $this->assertIsArray($createdCategory);

        $this->assertArrayHasKey('name', $createdCategory);


        $client->request('POST', '/api/admin/products/create', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
            'CONTENT_TYPE' => 'application/json',
        ], json_encode([
            'name' => 'Test product',
            'description' => 'Test product description',
            'price' => 100,
            'reference' => '0001XXX',
            'category' => $createdCategory['id']
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode(),$client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $createdProduct = json_decode($content, true);

        $this->assertIsArray($createdProduct);

        $this->assertArrayHasKey('name', $createdProduct);
        $this->assertEquals('Test product', $createdProduct['name']);
        $this->assertEquals('0001XXX', $createdProduct['reference']);
    }

    public function testGetProduct(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/products/find-all', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode(),$client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $products = json_decode($content, true);

        $this->assertIsArray($products);

        $this->assertArrayHasKey('id', $products[0]);
        $this->assertArrayHasKey('name', $products[0]);
    }

    public function testGetProductByCategory(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/products/find-by-category/1', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode(),$client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $products = json_decode($content, true);

        $this->assertIsArray($products);

        $this->assertArrayHasKey('id', $products[0]);
        $this->assertArrayHasKey('name', $products[0]);
    }

    public function testGetProductByBrand(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/products/find-by-brand/1', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode(),$client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $products = json_decode($content, true);

        $this->assertIsArray($products);

        $this->assertArrayHasKey('id', $products[0]);
        $this->assertArrayHasKey('name', $products[0]);
    }

    public function testGetByRefProduct(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/products/find-by-reference/0001XXX', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode(),$client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $product = json_decode($content, true);

        $this->assertIsArray($product);

        $this->assertArrayHasKey('id', $product);
        $this->assertArrayHasKey('name', $product);
    }

    public function testGetByIdProduct(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/products/find-by-reference/0001XXX', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode(),$client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $product = json_decode($content, true);

        $this->assertIsArray($product);

        $this->assertArrayHasKey('id', $product);
        $this->assertArrayHasKey('name', $product);

        $client->request('GET', '/api/products/find-by-id/' . $product['id'], [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode(),$client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $product = json_decode($content, true);

        $this->assertIsArray($product);

        $this->assertArrayHasKey('id', $product);
        $this->assertArrayHasKey('name', $product);
    }


    public function testUpdateProduct(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/products/find-all', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $products = json_decode($content, true);

        $this->assertIsArray($products);

        $brandToUpdate = $products[count($products) - 1];

        $client->request('PUT', '/api/admin/products/update/' . $brandToUpdate['id'], [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
            'CONTENT_TYPE' => 'application/json',
        ], json_encode([
            'name' => 'Test product modified',
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), $client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $updatedProduct = json_decode($content, true);

        $this->assertArrayHasKey('name', $updatedProduct);
        $this->assertEquals('Test product modified', $updatedProduct['name']);
    }

    public function testDeleteProduct(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/products/find-all', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $products = json_decode($content, true);

        $this->assertIsArray($products);

        $productsToSearch = $products[count($products) - 1];

        $client->request('DELETE', '/api/admin/products/delete/' . $productsToSearch['id'], [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), $client->getResponse()->getContent());
    }

}

