<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TaxesControllerTest extends WebTestCase
{
    private function getToken(KernelBrowser $client): string {
        $client->request('POST', '/api/login_check', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'username' => 'admin@localhost.local',
            'password' => 'adminpass1',
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        
        $data = json_decode($client->getResponse()->getContent(), true);
        return $data['token']; 
    }

    public function testCreateTax(): void {
        $client = static::createClient();

        $token = $this->getToken($client);


        $client->request('POST', '/api/admin/taxes/create', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
            'CONTENT_TYPE' => 'application/json',
        ], json_encode([
            'value' => 100,
            'description' => 'New Tax',
            'enabled' => true
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode(),$client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $createdTax = json_decode($content, true);

        $this->assertIsArray($createdTax);

        $this->assertArrayHasKey('id', $createdTax);
        $this->assertArrayHasKey('description', $createdTax);
        $this->assertEquals('New Tax', $createdTax['description']);
        $this->assertEquals(100, $createdTax['value']);
        $this->assertEquals(true, $createdTax['enabled']);
    }

    public function testGetTaxes(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/taxes/find-all', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode(),$client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $taxes = json_decode($content, true);

        $this->assertIsArray($taxes);

        $this->assertArrayHasKey('id', $taxes[0]);
        $this->assertArrayHasKey('value', $taxes[0]);
    }

    

    public function testUpdateTax(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/taxes/find-all', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $taxes = json_decode($content, true);

        $this->assertIsArray($taxes);

        $taxToUpdate = $taxes[count($taxes) - 1];

        $client->request('PUT', '/api/admin/taxes/update/' . $taxToUpdate['id'], [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
            'CONTENT_TYPE' => 'application/json',
        ], json_encode([
            'enabled' => false,
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), $client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $updatedTax = json_decode($content, true);

        $this->assertArrayHasKey('id', $updatedTax);
        $this->assertArrayHasKey('description', $updatedTax);
        $this->assertEquals(false, $updatedTax['enabled']);
    }

    public function testDeleteTax(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/taxes/find-all', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $taxes = json_decode($content, true);

        $this->assertIsArray($taxes);

        $taxToSearch = $taxes[count($taxes) - 1];

        $client->request('DELETE', '/api/admin/taxes/delete/' . $taxToSearch['id'], [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), $client->getResponse()->getContent());
    }

}

