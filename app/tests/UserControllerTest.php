<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    private function getToken(KernelBrowser $client): string {
        $client->request('POST', '/api/login_check', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'username' => 'admin@localhost.local',
            'password' => 'adminpass1',
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        
        $data = json_decode($client->getResponse()->getContent(), true);
        return $data['token']; 
    }

    public function testCreateUser(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $uniqueEmail = 'newuser' . time() . '@localhost.local';

        $client->request('POST', '/api/admin/users/create', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
            'CONTENT_TYPE' => 'application/json',
        ], json_encode([
            'email' => $uniqueEmail,
            'name' => 'New User',
            'roles' => ['ROLE_USER'],
            'password' => 'newpassword1'
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode(),$client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $createdUser = json_decode($content, true);

        $this->assertIsArray($createdUser);

        $this->assertArrayHasKey('id', $createdUser);
        $this->assertArrayHasKey('name', $createdUser);
        $this->assertEquals('New User', $createdUser['name']);
        $this->assertEquals($uniqueEmail, $createdUser['email']);
        $this->assertContains('ROLE_USER', $createdUser['roles']);
    }

    public function testGetUsers(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/admin/users/find-all', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode(),$client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $users = json_decode($content, true);

        $this->assertIsArray($users);

        $this->assertArrayHasKey('id', $users[0]);
        $this->assertArrayHasKey('name', $users[0]);
    }

    

    public function testUpdateUser(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/admin/users/find-all', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $users = json_decode($content, true);

        $this->assertIsArray($users);

        $userToUpdate = $users[count($users) - 1];

        $client->request('PUT', '/api/admin/users/update/' . $userToUpdate['id'], [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
            'CONTENT_TYPE' => 'application/json',
        ], json_encode([
            'name' => 'Updated Name',
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), $client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $updatedUser = json_decode($content, true);

        $this->assertArrayHasKey('id', $updatedUser);
        $this->assertArrayHasKey('name', $updatedUser);
        $this->assertEquals('Updated Name', $updatedUser['name']);
    }

    public function testGetUserByEmail(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/admin/users/find-all', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $users = json_decode($content, true);

        $this->assertIsArray($users);

        $userToSearch = $users[count($users) - 1];

        $client->request('GET', '/api/admin/users/find-by-email/' . $userToSearch['email'], [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), $client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $users = json_decode($content, true);

        $this->assertIsArray($users);

        $this->assertArrayHasKey('id', $users);
        $this->assertArrayHasKey('name', $users);
    }

    public function testGetUserById(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/admin/users/find-all', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $users = json_decode($content, true);

        $this->assertIsArray($users);

        $userToSearch = $users[count($users) - 1];

        $client->request('GET', '/api/admin/users/find-by-id/' . $userToSearch['id'], [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $users = json_decode($content, true);

        $this->assertIsArray($users);

        $this->assertArrayHasKey('id', $users);
        $this->assertArrayHasKey('name', $users);
    }

    public function testDeleteUser(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/admin/users/find-all', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $users = json_decode($content, true);

        $this->assertIsArray($users);

        $userToSearch = $users[count($users) - 1];

        $client->request('DELETE', '/api/admin/users/delete/' . $userToSearch['id'], [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), $client->getResponse()->getContent());
    }

}

