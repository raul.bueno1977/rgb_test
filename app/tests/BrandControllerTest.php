<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BrandControllerTest extends WebTestCase
{
    private function getToken(KernelBrowser $client): string {
        $client->request('POST', '/api/login_check', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'username' => 'admin@localhost.local',
            'password' => 'adminpass1',
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        
        $data = json_decode($client->getResponse()->getContent(), true);
        return $data['token']; 
    }

    public function testCreateBrand(): void {
        $client = static::createClient();

        $token = $this->getToken($client);


        $client->request('POST', '/api/admin/brands/create', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
            'CONTENT_TYPE' => 'application/json',
        ], json_encode([
            'name' => 'Test brand'
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode(),$client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $createdBrand = json_decode($content, true);

        $this->assertIsArray($createdBrand);

        $this->assertArrayHasKey('name', $createdBrand);
        $this->assertEquals('Test brand', $createdBrand['name']);
    }

    public function testGetBrand(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/brands/find-all', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode(),$client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $brands = json_decode($content, true);

        $this->assertIsArray($brands);

        $this->assertArrayHasKey('id', $brands[0]);
        $this->assertArrayHasKey('name', $brands[0]);
    }


    public function testUpdateBrand(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/brands/find-all', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $brands = json_decode($content, true);

        $this->assertIsArray($brands);

        $brandToUpdate = $brands[count($brands) - 1];

        $client->request('PUT', '/api/admin/brands/update/' . $brandToUpdate['id'], [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
            'CONTENT_TYPE' => 'application/json',
        ], json_encode([
            'name' => 'Test brand modified',
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), $client->getResponse()->getContent());

        $content = $client->getResponse()->getContent();
        $updatedBrand = json_decode($content, true);

        $this->assertArrayHasKey('name', $updatedBrand);
        $this->assertEquals('Test brand modified', $updatedBrand['name']);
    }

    public function testDeleteBrand(): void {
        $client = static::createClient();

        $token = $this->getToken($client);

        $client->request('GET', '/api/brands/find-all', [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $brands = json_decode($content, true);

        $this->assertIsArray($brands);

        $brandsToSearch = $brands[count($brands) - 1];

        $client->request('DELETE', '/api/admin/brands/delete/' . $brandsToSearch['id'], [], [], [
            'HTTP_Authorization' => sprintf('Bearer %s', $token),
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), $client->getResponse()->getContent());
    }

}

